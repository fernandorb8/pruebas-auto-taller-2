import {browser, by, element, ElementFinder, promise, protractor} from 'protractor';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  searchHeroInHeroSearch(heroSearched: string){
    return element(by.tagName('input')).sendKeys(heroSearched)
  }

  getSearchResultList(){
    return element.all(by.css('.search-result')).getText();
  }

  deleteHero(heroName: string){
    element(by.tagName('my-heroes')).all(by.tagName('li')).then((elements) => {
      for (let elem of elements){
        elem.all(by.tagName('span')).get(1).getText().then((text) => {
          if(text === heroName){
            elem.element(by.tagName('button')).click()
          }
        })
      }
    })
  }

  editHero(heroName: string, newHeroName: string){
    element(by.tagName('my-heroes')).all(by.tagName('li')).then((elements) => {
      let sElem;
      for (let elem of elements){
        elem.all(by.tagName('span')).get(1).getText().then((text) => {
          if(text === heroName){
            elem.click()
          }
        })
      }
    }).then(() => {
      element(by.buttonText('View Details')).click()
      element(by.tagName('input')).clear()
      element(by.tagName('input')).sendKeys(newHeroName)
      element(by.buttonText('Save')).click()
      let until = protractor.ExpectedConditions;
      browser.wait(until.presenceOf(element(by.css('.heroes'))), 10000, 'Element taking too long to appear in the DOM');
    })
  }

  getHeroList() {
    return element(by.tagName('my-heroes')).all(by.tagName('span')).filter( (elem, index) => {
      return elem.getAttribute('class').then((classr) => {
        return classr === '';
      })
    }).getText()
  }

  goToDashboardHero(){
    element(by.css('.module.hero')).click()
  }

  getHeroName(){
    return element(by.tagName('h2')).getText()
  }

  goToListHero(){
    element(by.tagName('my-heroes')).all(by.tagName('li')).first().click()
    element(by.buttonText('View Details')).click()
  }

  goToSearchHeroResult(heroName: string){
    element.all(by.css('.search-result')).each(elem => {
      elem.getText().then(text => {
        if(text === heroName){
          elem.click()
        }
      })
    })
  }

}
