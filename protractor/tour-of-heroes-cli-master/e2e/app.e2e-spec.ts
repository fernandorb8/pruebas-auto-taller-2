import { TourOfHeroesPage } from './app.po';

describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });

  it('should search hero', () => {
    page.navigateTo();
    page.searchHeroInHeroSearch("N");
    expect(page.getSearchResultList()).toEqual(['Mr. Nice', 'Narco', 'Magneta', 'RubberMan', 'Dynama', 'Tornado'])
  });

  it('should go to dashboard hero', () => {
    page.navigateTo();
    page.goToDashboardHero()
    expect(page.getHeroName()).toEqual('Mr. Nice details!')
  });

  it('should go to search hero', () => {
    page.navigateTo();
    page.searchHeroInHeroSearch('Zero');
    page.goToSearchHeroResult('Zero');
    expect(page.getHeroName()).toEqual('Zero details!')
  })

});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

  it('should delete a hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.deleteHero('Zero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n - 1));
  });

  it('should edit a hero', () => {
    page.editHero('Bombasto', 'One');
    expect(page.getHeroList()).toContain('One');
  });

  it('should got to list hero', () => {
    page.goToListHero()
    expect(page.getHeroName()).toEqual('Mr. Nice details!')
  });

});
