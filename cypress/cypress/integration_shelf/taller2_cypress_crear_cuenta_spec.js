describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
      cy.visit('https://losestudiantes.co')

      cy.contains('Cerrar').click()

      //Crear cuenta.
      cy.contains('Ingresar').click()
      cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Fernando")
      cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Reyes Bejarano")
      cy.get('.cajaSignUp').find('input[name="correo"]').click().type("fernandorb_8@hotmail.com")
      cy.get('.cajaSignUp').find('select[name="idPrograma"]').select("Administración")
      cy.get('.cajaSignUp').find('input[name="password"]').type("123456789abcd")
      cy.get('.cajaSignUp').find('input[name="acepta"]').check()
      cy.contains('Registrarse').click()
    })
})
