module.exports = { // adapted from: https://git.io/vodU0
  'Los estudiantes login falied': function(browser) {
    browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .waitForElementVisible('.botonIngresar', 4000)
      .click('.botonIngresar')
      .setValue('.cajaLogIn input[name="correo"]', 'wrongemail@example.com')
      .setValue('.cajaLogIn input[name="password"]', '1234')
      .click('.cajaLogIn .logInButton')
      .waitForElementVisible('.aviso.alert.alert-danger', 4000)
      .assert.containsText('.aviso.alert.alert-danger', 'El correo y la contraseña que ingresaste no figuran')
      .end();
  },
  'Los estudiates go to teacher page': function(browser){
    browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .waitForElementVisible('.botonIngresar', 4000)
      .click('.Select-placeholder')
      .setValue('.Select-input input', 'Mario Linares Vasquez')
      .waitForElementVisible('.Select-option', 4000)
      .useXpath()
      .click("//div[text()='Mario Linares Vasquez - Ingeniería de Sistemas']")
      .useCss()
      .waitForElementVisible('.nombreProfesor', 4000)
      .assert.containsText('.nombreProfesor', 'Mario Linares Vasquez')
      .end()
  },
  'Los estudiates filter teacher\'s page by course': function(browser){
    browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .waitForElementVisible('.botonIngresar', 4000)
      .click('.Select-placeholder')
      .setValue('.Select-input input', 'Mario Linares Vasquez')
      .waitForElementVisible('.Select-option', 4000)
      .useXpath()
      .click("//div[text()='Mario Linares Vasquez - Ingeniería de Sistemas']")
      .useCss()
      .waitForElementVisible('input[name="id:MISO4208"]', 4000)
      .click('input[name="id:MISO4208"]')
      .expect.element('input[name="id:MISO4208"]').to.be.selected;
    browser.end()
  }

};
