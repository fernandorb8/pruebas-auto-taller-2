var assert = require('assert');
describe('los estudiantes login ', function() {
    it('should visit los estudiantes and create account', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar');
        browser.click('button=Ingresar');

        var cajaSignUp = browser.element('.cajaSignUp');

        var nameInput = cajaSignUp.element('input[name="nombre"]')
        nameInput.click();
        nameInput.keys("Fernando")

        var lastNameInput = cajaSignUp.element('input[name="apellido"]')
        lastNameInput.click()
        lastNameInput.keys("Reyes Bejarano")

        var mailInput = cajaSignUp.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('fernandorb_8@hotmail.com');

        browser.selectByVisibleText('select[name="idPrograma"]','Administración')

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123456789abcd');

        cajaSignUp.element('input[name="acepta"]').click()

        cajaSignUp.element('button=Registrarse').click()

        browser.waitForVisible('.sweet-alert');
        var alerts = browser.element('.sweet-alert').elements('div').value
        var alertText = alerts[1].getText();

        expect(alertText).toBeDefined("Error: Ya existe un usuario registrado con el correo 'fernandorb_8@hotmail.com'");
    });

    it('should visit los estudiantes and fail at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.waitForVisible('button=Ingresar');
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger');

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });

    it('should visit los estudiantes and log in', function () {
        browser.url('https://losestudiantes.co');
        browser.waitForVisible('button=Ingresar');
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('fernandorb_8@hotmail.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('123456789abcd');

        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('#cuenta');

        var alertText = browser.element('#cuenta').getText();
        expect(alertText).toBeDefined('No existe el logo de inicio de sesión.');
    });

    it('should visit los estudiantes and search teacher', function () {
        browser.url('https://losestudiantes.co');

        browser.element('.Select-placeholder').click()
        browser.elementActive().keys("Mario Linares Vasquez")

        browser.waitForVisible('.Select-option');
        expect(browser.element('div=Mario Linares Vasquez - Ingeniería de Sistemas')).toBeDefined('No se encontró el profesor.')
    });

});

describe('los estudiantes login correcto', function() {
});
